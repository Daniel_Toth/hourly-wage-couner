<?php
/** The name of the database for WorkTracker */
define( 'DB_NAME', 'worktracker' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

date_default_timezone_set("Europe/Bratislava");

$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// Check connection
if (mysqli_connect_error()) {
    die("Database connection failed: " . mysqli_connect_error());
}

/** Installation */

// sql to create table
$sql = "CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `startdate` datetime DEFAULT NULL,
  `seconds` int(10) UNSIGNED NOT NULL,
  `deadline` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `wage` (
  `id` int(11) NOT NULL,
  `wage` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `wage` (`id`, `wage`) VALUES (1, 10);
ALTER TABLE `projects` ADD PRIMARY KEY (`id`);
ALTER TABLE `wage` ADD PRIMARY KEY (`id`);
ALTER TABLE `projects` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `wage` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `projects` CHANGE `startdate` `startdate` DATETIME NULL DEFAULT NULL;
ALTER TABLE `projects` CHANGE `deadline` `deadline` DATETIME NULL DEFAULT NULL;";

if (mysqli_multi_query($conn, $sql)) {
  echo "<h1>Installation is successful</h1><input type=\"button\" value=\"Next\" onClick=\"window.location.href=window.location.href\">";
  exit;
}
