<?php
include 'config.php';
if (!empty($_POST["name"])) {
  $name = $_POST['name'];
  $deadline = NULL;
  if (!empty($_POST["deadline"])) {
    $deadline = $_POST['deadline'].":00";
  }
  $sql = "INSERT INTO `projects` (`id`, `name`, `startdate`, `seconds`, `deadline`) VALUES (NULL, '" . $name . "', NULL, '0', '" . $deadline . "')";
echo $sql;
  if (mysqli_query($conn, $sql)) {
    echo "Table MyGuests created successfully";
    Header( "HTTP/1.1 301 Moved Permanently" );
    Header( "Location: index.php");
    exit;
  } else {
    echo "Error creating table: " . mysqli_error($conn);
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <script src="./js/datetimepicker_css.js"></script>
    <script src="./js/jquery-1.12.4.min.js"></script>
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
    <form class="addproject" action="index.php" method="post">
      <label for="">Deadline</label>
      <input type="text" name="name" placeholder="Name of the Project">
      <label for="">Deadline</label>
      <div class="datepicker">
          <input type="Text" id="dateinput" name="deadline" placeholder="Deadline" maxlength="25" size="25"/>
          <img src="pics/datepicker/cal.gif" onclick="javascript:NewCssCal('dateinput')" style="cursor:pointer"/>
      </div>
      <input type="submit" value="Add">
    </form>
    <div class="wrapper">
      <div class="hourly-wage">
        <label for="hourly-wage">Hourly wage :</label>
        <input type="text" id="hourly-wage" value="<?php
        $sql = "SELECT `wage` FROM `wage`";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_assoc($result);
        echo $row["wage"];
        ?>">
        &#8364; / hour
      </div>
      <table class="project">
        <tr>
          <th>Project name</th>
          <th>Deadline</th>
          <th>Work Time</th>
          <th>Begin</th>
          <th></th>
          <th></th>
          <th>&#8364;</th>
        </tr>
<?php
$sql = "SELECT * FROM projects";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {?>

      <tr class="row_<?=$row["id"]?>">
        <td><?=$row["name"]?></td>
        <td><?=$row["deadline"]?></td>
        <td class="seconds_<?=$row["id"]?>" value="<?=$row["seconds"] ?>">
          <?php
          $time = gmdate("H:i:s", $row["seconds"]);
          echo $time;
          ?>
        </td>
        <td class="begin_<?=$row["id"]?>">
          <?=$row["startdate"]?>
        </td>
        <td>
          <div class="start" value="<?=$row["id"]?>">
            start
          </div>
        </td>
        <td>
          <div class="delete" value="<?=$row["id"]?>">
            delete
          </div>
        </td>
        <td class="wage_<?=$row["id"]?>">
          5
        </td>
      </tr>

<?php }
} else {
    echo "0 results";
}

mysqli_close($conn);


?>
    </table>
</div>
    <script type="text/javascript">
    $(document).ready(function() {
    	var timerarray = [];
    	var lastelapsedsecs = [];

    	function secondsToHms(d) {
    		d = Number(d);
    		var h = Math.floor(d / 3600);
    		var m = Math.floor(d % 3600 / 60);
    		var s = Math.floor(d % 3600 % 60);
    		var hDisplay = h < 10 ? "0" + h : h;
    		var mDisplay = m < 10 ? "0" + m : m;
    		var sDisplay = s < 10 ? "0" + s : s;
    		return hDisplay + ":" + mDisplay + ":" + sDisplay;
    	}
    	$("#hourly-wage").keyup(function() {
    		var hourwage = document.getElementById("hourly-wage").value;
    		$(".start").each(function(index, element) {
    			var rowid = $(this).attr("value");
    			var wagemoney = ($(".seconds_" + rowid).attr("value") / 3600) * hourwage;
    			$(".wage_" + rowid).html(wagemoney.toFixed(2));
    		});
    		$.post("ajax.php", {
    				id: hourwage,
    				command: "wage"
    			},
    			function(data, status) {
    				if (status == "success") {
    					console.log(hourwage);
    				} else {
    					alert("Lost Connection! ");
    				}
    			});
    	});
    	$("#hourly-wage").keyup();
    	$(".start").each(function(index, element) {
    		var rowid = $(this).attr("value");
    		console.log($(".begin_" + rowid).html());
    		if ($(".begin_" + rowid).html().trim() != "") {
    			$(this).html('stop');
    			$(this).css({
    				'backgroundColor': '#22d'
    			});
    			timerarray[rowid] = setInterval(function() {
    				var startdatestring = $(".begin_" + rowid).html().trim();
    				var datearr = startdatestring.split(/[- :]/);
    				var endDate = new Date();
    				var startDate = new Date(datearr[0], datearr[1] - 1, datearr[2], datearr[3], datearr[4], datearr[5], 0);

    				if (lastelapsedsecs[rowid] == undefined) {
    					lastelapsedsecs[rowid] = $(".seconds_" + rowid).attr("value");

    					console.log(lastelapsedsecs[rowid]);
    				}
    				var elapsedseconds = Math.floor((endDate.getTime() - startDate.getTime()) / 1000) + Number(lastelapsedsecs[rowid]);
    				var seconds = secondsToHms(elapsedseconds);
    				$(".seconds_" + rowid).html(seconds);
    				$(".seconds_" + rowid).attr("value", elapsedseconds);

    				var wagemoney = (elapsedseconds / 3600) * document.getElementById("hourly-wage").value;
    				$(".wage_" + rowid).html(wagemoney.toFixed(2));
    			}, 1000);
    		}
    	});

    	$(".start").click(function() {
    		var rowid = $(this).attr("value");
    		console.log($(".begin_" + rowid).html());
    		if ($(".begin_" + rowid).html().trim() == "") {
    			$(this).html('stop');
    			$(this).css({
    				'backgroundColor': '#22d'
    			});
    			timerarray[rowid] = setInterval(function() {
    				var startdatestring = $(".begin_" + rowid).html();
    				var datearr = startdatestring.split(/[- :]/);
    				var endDate = new Date();
    				var startDate = new Date(datearr[0], datearr[1] - 1, datearr[2], datearr[3], datearr[4], datearr[5], 0);
    				if (lastelapsedsecs[rowid] == undefined) {
    					lastelapsedsecs[rowid] = $(".seconds_" + rowid).attr("value");
    					console.log(lastelapsedsecs[rowid]);
    				}
    				var elapsedseconds = Math.floor((endDate.getTime() - startDate.getTime()) / 1000) + Number(lastelapsedsecs[rowid]);
    				var seconds = secondsToHms(elapsedseconds);
    				$(".seconds_" + rowid).html(seconds);
    				$(".seconds_" + rowid).attr("value", elapsedseconds);
    				var wagemoney = (elapsedseconds / 3600) * document.getElementById("hourly-wage").value;
    				$(".wage_" + rowid).html(wagemoney.toFixed(2));
    			}, 1000);
    			$.post("ajax.php", {
    					id: rowid,
    					command: "start"
    				},
    				function(data, status) {
    					if (status == "success") {
    						console.log("begin_" + rowid);
    						$(".begin_" + rowid).html(data);
    					} else {
    						alert("Lost Connection! ");
    					}
    				});
    		} else {
    			$(this).html('start');
    			$(this).css({
    				'backgroundColor': '#2d2'
    			});
    			clearTimeout(timerarray[rowid]);
    			$(".begin_" + rowid).html("");
    			var elapsedsecforsave = $(".seconds_" + rowid).attr('value');
    			lastelapsedsecs[rowid] = elapsedsecforsave;
    			console.log("stopped : " + lastelapsedsecs[rowid]);


    			$.post("ajax.php", {
    					id: rowid,
    					command: "save",
    					seconds: elapsedsecforsave
    				},
    				function(data, status) {
    					if (status == "success") {
    						console.log("itt a valasz:" + data);

    					} else {

    						alert("Lost Connection! ");
    					}
    				});
    		}
    	});
    	$(".delete").click(function() {
    		var rowid = $(this).attr("value");
    		$.post("ajax.php", {
    				id: rowid,
    				command: "delete"
    			},
    			function(data, status) {
    				if (status == "success") {
    					console.log("begin_" + rowid);
    					$(".row_" + rowid).hide();
    				} else {
    					alert("Lost Connection! ");
    				}
    			});
    	});
    });
    </script>
  </body>
</html>
